import React, { Component } from 'react';
import '../styles/App.css';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { logout } from './../actions/'

class Navbar extends Component {

  static propTypes = {
    loginSuccessful: PropTypes.bool.required,
    logout: PropTypes.func.required,
  }

  //Removed constructor and internal state.
  render() {
    const {
      loginSuccessful,
      logout
    } = this.props;

    return (
      <div className='app-navbar'>
        <div className='flex-container'>
          <div className='header'>React Debug App</div>
          {loginSuccessful &&
            <button className='flat-button border-gray' onClick={(e) => logout()}>Sign Out</button>
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ loginSuccessful }, ownProps) => {
  return {
    loginSuccessful,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    logout: () => dispatch(logout()),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Navbar);