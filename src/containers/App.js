import React, { Component } from 'react';
import { connect } from 'react-redux'
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import LoginForm from '../components/LoginForm';
//Added missing `LoginForm` import.
import { Glyphicon } from 'react-bootstrap';
import PropTypes from 'prop-types';
import '../styles/App.css';

class App extends Component {

  static propTypes = {
    loginSuccessful: PropTypes.bool.required,
  }

  // Removed constructor and internal state.
  render() {
    const {
      loginSuccessful
    } = this.props;

    return (
      <div className='app'>
        <Navbar ref='navbutton' />
        {loginSuccessful ? (
          <div className='text-center mt9x'>
            <Glyphicon glyph='glyphicon glyphicon-ok-sign' />
            <h2>Great work!</h2>
          </div>
        ): (
          <LoginForm />
        )}
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = ({ loginSuccessful }, ownProps) => {
  return {
    loginSuccessful,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);