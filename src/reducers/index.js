const reducers = (state = {}, action) => {
  switch (action.type) {
    case 'SUCCESSFUL_LOGIN':
      return {
        ...state,
        loginSuccessful: true,
      };
    case 'LOGOUT':
      return {
        ...state,
        loginSuccessful: false,
      };
    default:
      return state
  }
}

export default reducers;