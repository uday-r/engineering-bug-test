const successfulLogin = () => ({
  type: 'SUCCESSFUL_LOGIN',
});

const logout = () => ({
  type: 'LOGOUT',
});

export {
  successfulLogin,
  logout,
}
