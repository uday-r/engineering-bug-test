import React, { Component } from 'react';
import '../styles/App.css';

class Footer extends Component {
  // Fixed typo `extend` to `extends`.
  render() {
    return (
      <div>
        <div className='footer-anchor'></div>
        <div className='app-footer'></div>
      </div>
    );
  }
}

export default Footer;